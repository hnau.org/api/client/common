package org.hnau.api.client.common

import org.hnau.api.common.response.ApiResponse
import org.hnau.api.common.response.error.exception


val <T> ApiResponse<T>.valueOrThrow
    get() = when (this) {
        is ApiResponse.Success -> value
        is ApiResponse.Error -> throw error.exception
    }