package org.hnau.api.client.common

import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.receiver.BytesReceiver


fun ApiClientEngine(
        writeRequest: suspend (BytesReceiver.() -> Unit) -> BytesProvider
) = object : ApiClientEngine {

    override suspend fun <O> call(
            call: ApiCall<O>
    ): O {
        val responseBytesProvider = writeRequest(call.writeRequest)
        return call.readResponse(responseBytesProvider)
    }

}