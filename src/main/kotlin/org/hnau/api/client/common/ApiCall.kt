package org.hnau.api.client.common

import org.hnau.api.common.endpoint.Endpoint
import org.hnau.api.common.endpoint.EndpointKey
import org.hnau.api.common.response.ApiResponse
import org.hnau.api.common.response.ApiResponseReadWriteFactory
import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.receiver.BytesReceiver
import org.hnau.base.data.bytes.receiver.write


data class ApiCall<O>(
        val writeRequest: BytesReceiver.() -> Unit,
        val readResponse: BytesProvider.() -> O
)

inline fun <I, O> ApiCall(
        endpoint: Endpoint<I, O>,
        param: I,
        apiResponseReadWriteFactory: ApiResponseReadWriteFactory,
        crossinline writeParam: BytesReceiver.(I) -> Unit,
        crossinline readResponse: BytesProvider.() -> O
) = ApiCall<ApiResponse<O>>(
        writeRequest = {
            write(endpoint.key, EndpointKey.bytesAdapter.write)
            write(param, writeParam)
        },
        readResponse = {
            apiResponseReadWriteFactory.read(
                    bytesProvider = this,
                    contentReader = readResponse
            )
        }
)