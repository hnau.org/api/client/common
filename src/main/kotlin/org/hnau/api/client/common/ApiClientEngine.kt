package org.hnau.api.client.common


interface ApiClientEngine {

    companion object;

    suspend fun <O> call(call: ApiCall<O>): O

}